#!/usr/bin/env python3
import os
import time
import sys
from sys import argv
import argparse
import tarfile
import shutil
from shutil import make_archive
import csv


def createParser():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--directory', default='home/vagrant/project')
    parser.add_argument('--output', default='home/vagrant/backup')
    parser.add_argument('-a', default='gztar')
    parser.add_argument('-j', default='./journal.csv')
    parser.add_argument('--help', action='help',
                        help="Данная программа используется для резервного копирования файлов\
   Вы можете выбрать следующие опции:\
         --direcroty - Каталог, файлы которого подлежат резервному копированию;\
         --output - Каталог для расположение архива после выполнения копирования;\
         -a - Тип сжатия, используемый в ходе архивирования: tar, gztar, zip, bztar, xztar;\
         -j - Расположения журнала\
         После выполнение архивации вы увидите абсолютный путь созданного архива.\
         В журнале записей вам будет доступна информация о месторасположении файлов, архива, время и иную информацию")


    return parser


if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args(sys.argv[1:])
    namespace1 = parser.parse_args(sys.argv[1:])
    namespace2 = parser.parse_args(sys.argv[1:])
    namespace3 = parser.parse_args(sys.argv[1:])


    for name in namespace.directory.split(): 
        files = format(name)
    for name in namespace1.output.split():
        backup = format(name)
    for name in namespace2.a.split():
        allowed_values = ["gztar", "zip", "bztar", "tar", "xztar"]
        while format(name) not in allowed_values:
            print("Неккоректные данные. Несуществующий алгоритм сжатия")
            exit()
        s = format(name)
    for name in namespace3.j.split():
        journal = format(name)



x = os.path.basename((files))


y = backup+"/"+x+time.strftime('_%Y-%m-%d-%H:%M:%S')



if os.path.exists(files) == True:
    if os.access(files, os.R_OK) == True:
        if os.path.exists(backup) == True:
            if os.access(backup, os.W_OK) == True:
                make_archive(y, s, files)
                z = make_archive(y, s, files)
                if os.path.isfile(z) == True:
                    checkfile = "SUCCSES"
                    print("Выполнено успешно")
                    print(z)
                    sys.stdout.write(z)
                else:
                    checkfile = "FAIL"
                    print("Файл не создан")
                    sys.stderr.write('Файл не создан')
            else:
                checkfile = "FAIL"
                print("Запись в указанный каталог запрещена")
                sys.stderr.write('Запись запрещена')
        else:
            checkfile = "FAIL"
            print("Путь для архива не найден")
            sys.stderr.write('Путь не найде')
    else:
        checkfile = "FAIL"
        print("Доступ запрещен")
        sys.stderr.write('Доступ запрещен')
else:
    checkfile = "FAIL"
    print("Каталога не существует")
    sys.stderr.write('Каталога не существует')

start_time = time.time()
during = time.time() - start_time

if os.path.isfile(journal) == False:
    myData = [["Backup", "Files", "time", "during", "status"],
        [backup, files, time.strftime('_%Y-%m-%d-%H-%M-%S'), during, checkfile]]
else:
    myData = [[backup, files, time.strftime('_%Y-%m-%d-%H-%M-%S'), during, checkfile]]




myFile = open(journal, 'a')
with myFile:
    writer = csv.writer(myFile)
    writer.writerows(myData)

#print("Выполнено успешно")